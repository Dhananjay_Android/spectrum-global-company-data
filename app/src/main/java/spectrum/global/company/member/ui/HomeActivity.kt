package spectrum.global.company.member.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import spectrum.global.company.member.R
import spectrum.global.company.member.ui.adapter.HomeAdapter
import spectrum.global.company.member.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityHomeBinding: ActivityHomeBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_home)

        val tabLayout: TabLayout = activityHomeBinding.tabLayout
        val viewPager: ViewPager = activityHomeBinding.viewPager
        tabLayout.addTab(tabLayout.newTab().setText("Company"))
        tabLayout.addTab(tabLayout.newTab().setText("Member"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = HomeAdapter(
            this,
            supportFragmentManager,
            tabLayout!!.tabCount
        )
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }
}