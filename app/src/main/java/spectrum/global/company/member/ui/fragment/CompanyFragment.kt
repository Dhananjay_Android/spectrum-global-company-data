package spectrum.global.company.member.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager

import androidx.fragment.app.Fragment

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import spectrum.global.company.member.R
import spectrum.global.company.member.ui.viewModel.MainViewModel

import spectrum.global.company.member.data.Companies
import spectrum.global.company.member.databinding.FragmentCompanyBinding
import spectrum.global.company.member.ui.adapter.CompanyDataAdapter


class CompanyFragment : Fragment() {
    private var mainViewModel: MainViewModel? = null
    private var companyDataAdapter: CompanyDataAdapter? = null
    lateinit var binding: FragmentCompanyBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCompanyBinding.inflate(inflater, container, false)
        return binding.root

    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val recyclerView = binding.rvCompany
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        binding.searchEditText.queryHint = getString(R.string.search_company)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        companyDataAdapter = CompanyDataAdapter()
        recyclerView.adapter = companyDataAdapter
        getAllCompanyData()
        searching(binding.searchEditText)
        binding.progress?.visibility = View.VISIBLE
        binding.text.visibility = View.GONE
        binding.toggleButton.visibility = View.GONE
        binding.text.text = getString(R.string.sort_company)
        binding.searchEditText.hideKeyboard()
        binding.toggleButton.setOnCheckedChangeListener { compoundButton, b ->
            companyDataAdapter?.sortMembers("name", b)


        }
    }

    private fun getAllCompanyData() {
        mainViewModel?.allInfo?.observe(this,
            Observer<List<Any>> {
                    employees -> companyDataAdapter?.setEmployeeList(employees as ArrayList<Companies>)
                binding.progress.visibility = View.GONE
                binding.text.visibility = View.VISIBLE
                binding.toggleButton.visibility = View.VISIBLE
            })
    }

    private fun searching(search: SearchView) {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                companyDataAdapter?.filter?.filter(newText)
                return true
            }
        })

    }

    private fun View.hideKeyboard() {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

}