package spectrum.global.company.member.network

import retrofit2.http.GET
import retrofit2.Call
import spectrum.global.company.member.data.Companies

interface CompanyDataService {
    @GET("api/json/get/Vk-LhK44U")
    fun getCompany(): Call<ArrayList<Companies>>
}