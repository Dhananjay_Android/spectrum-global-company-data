package spectrum.global.company.member.network

import android.util.Log

import androidx.lifecycle.MutableLiveData

import java.util.ArrayList

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import spectrum.global.company.member.data.Companies

class CompanyRepository {
    private val mutableLiveData = MutableLiveData<ArrayList<Companies>>()

    fun getMutableLiveData(): MutableLiveData<ArrayList<Companies>> {

        val userDataService = RetrofitClient.service
        val call = userDataService.getCompany()
        call.enqueue(object : Callback<ArrayList<Companies>> {
            override fun onResponse(
                call: Call<ArrayList<Companies>>,
                response: Response<ArrayList<Companies>>
            ) {
                val employeeDBResponse = response.body()
                if (employeeDBResponse != null) {
                    mutableLiveData.value = employeeDBResponse
                }
            }

            override fun onFailure(call: Call<ArrayList<Companies>>, t: Throwable) {
                Log.d("DD@@E", ">>" + t.message)
            }

        })

        return mutableLiveData
    }

    companion object {
        private val TAG = "EmployeeRepository"
    }
}
