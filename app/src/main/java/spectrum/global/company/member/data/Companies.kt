package spectrum.global.company.member.data
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.annotations.SerializedName
import spectrum.global.company.member.R

data class Companies(
    @SerializedName("about")
    var about: String?,
    @SerializedName("company")
    var company: String?,
    @SerializedName("_id")
    var id: String?,
    @SerializedName("logo")
    var logo: String?,
    @SerializedName("members")
    var members: ArrayList<Member>?,
    @SerializedName("website")
    var website: String?
)

data class Member(
    @SerializedName("age")
    var age: Int?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("_id")
    var id: String?,
    @SerializedName("name")
    var name: Name?,
    @SerializedName("phone")
    var phone: String?
)

data class Name(
    @SerializedName("first")
    var first: String?,
    @SerializedName("last")
    var last: String?


)
@BindingAdapter("imageUrl")
fun setLogo(imageView : ImageView, url: String) {
    Glide.with(imageView)
        .apply { RequestOptions()
            .placeholder(R.drawable.placeholder)}
        .load(url)
        .into(imageView)
}