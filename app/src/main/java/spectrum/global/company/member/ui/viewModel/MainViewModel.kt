package spectrum.global.company.member.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import spectrum.global.company.member.data.Companies
import spectrum.global.company.member.network.CompanyRepository


class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val companyRepository: CompanyRepository = CompanyRepository()

    val allInfo: LiveData<ArrayList<Companies>>
        get() = companyRepository.getMutableLiveData()

}