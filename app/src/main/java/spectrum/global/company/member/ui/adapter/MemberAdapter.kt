package spectrum.global.company.member.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import io.realm.Realm
import spectrum.global.company.member.R
import spectrum.global.company.member.data.Companies
import spectrum.global.company.member.data.Member
import spectrum.global.company.member.databinding.ViewMemberItemBinding
import spectrum.global.company.member.realmDb.CompanyDb
import java.util.*

class MemberAdapter : RecyclerView.Adapter<MemberAdapter.MemberViewHolder>(), Filterable {
    private var filteredMember: ArrayList<Member>? = null
    private var member: ArrayList<Member>? = null
    private var isFavorite: Boolean? = false


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MemberViewHolder {
        val viewMemberItemBinding = DataBindingUtil.inflate<ViewMemberItemBinding>(
            LayoutInflater.from(viewGroup.context),
            R.layout.view_member_item, viewGroup, false
        )
        return MemberViewHolder(viewMemberItemBinding)
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) = holder.bind(filteredMember?.get(position)!!, position)

    override fun getItemCount(): Int {
        return filteredMember?.size ?: 0
    }

    fun setEmployeeList(companies: ArrayList<Companies>) {
        val member = ArrayList<Member>()
        companies.forEach { company -> company.members?.let { member.addAll(it) } }
        this.member = member
        this.filteredMember = member
        notifyDataSetChanged()
    }

    fun sortMembers(sortField: String, order: Boolean) {
        when (sortField) {
            "name" -> {
                if (order /*== Sort.DESC*/)
                    filteredMember?.sortWith(compareByDescending { it.name?.first + it.name?.last })
                else
                    filteredMember?.sortWith(compareBy { it.name?.first + it.name?.last })
                notifyDataSetChanged()
            }
            "age" -> {
                if (order/* == Sort.DESC*/)
                    filteredMember?.sortWith(compareByDescending { it.age })
                else
                    filteredMember?.sortWith(compareBy { it.age })
                notifyDataSetChanged()
            }
        }
    }

    inner class MemberViewHolder(val binding: ViewMemberItemBinding) :
        RecyclerView.ViewHolder(binding.root){
        fun bind(item: Member, position: Int) {
            binding.member = item
            val realm = Realm.getDefaultInstance()
            val memberDb =
                realm.where(CompanyDb::class.java).equalTo("companyId", item?.id)
                    .findFirst()

            if (memberDb?.isMemberFav == true) {
                binding?.ivFav.setImageResource(R.drawable.ic_favorite_icon)
            } else {
                binding?.ivFav.setImageResource(R.drawable.ic_unfavorite_icon)

            }

            binding.ivFav.setOnClickListener {
                val member: CompanyDb = CompanyDb()
                if (isFavorite == false) {
                    binding.ivFav.setImageResource(R.drawable.ic_favorite_icon)
                    var realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        member?.id = UUID.randomUUID().toString()
                        member?.companyId = item?.id
                        member.isMemberFav = true
                        realm.copyToRealmOrUpdate(member)
                    }
                    isFavorite = true
                    realm.close()
                } else {
                    binding.ivFav.setImageResource(R.drawable.ic_unfavorite_icon)
                    var realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        member?.id = UUID.randomUUID().toString()
                        member?.companyId = item?.id
                        member.isMemberFav = false
                        realm.copyToRealmOrUpdate(member)
                    }
                    realm.close()
                    isFavorite = false
                }

            }
        }

    }

    enum class Sort {
        ASC, DESC
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("DefaultLocale")
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredMember = member
                } else {
                    val filteredList = ArrayList<Member>()
                    member?.forEach {
                        if (it.name.toString().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(it)
                        }
                    }
                    filteredMember = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredMember
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: Filter.FilterResults
            ) {
                filteredMember = null
                filteredMember = filterResults.values as ArrayList<Member>

                notifyDataSetChanged()
            }
        }
    }

}