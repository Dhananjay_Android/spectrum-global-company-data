package spectrum.global.company.member.realmDb

import io.realm.annotations.PrimaryKey

import io.realm.RealmObject
import io.realm.annotations.Required
open class CompanyDb:RealmObject(){
    @PrimaryKey
    @Required
    var id:String?= null
    var companyId:String? = null
    var isFav:Boolean? = false
    var isFollow:Boolean?= false
    var isMemberFav:Boolean?= false
}

