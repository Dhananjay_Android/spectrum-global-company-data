package spectrum.global.company.member

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApplication :Application(){
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("spectrum.realm")
            .schemaVersion(0)
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.setDefaultConfiguration(config)
    }
}