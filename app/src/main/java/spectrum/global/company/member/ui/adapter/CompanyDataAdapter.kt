package spectrum.global.company.member.ui.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

import spectrum.global.company.member.data.Companies
import spectrum.global.company.member.databinding.ViewCompanyItemBinding
import io.realm.Realm
import spectrum.global.company.member.R
import spectrum.global.company.member.realmDb.CompanyDb
import java.util.*




class CompanyDataAdapter : RecyclerView.Adapter<CompanyDataAdapter.CompanyViewHolder>(),
    Filterable {

    private var employeeFiltered: List<Companies>? = null
    private var employees: ArrayList<Companies>? = null
    private var isFavorite: Boolean? = false
    private var isFollowing: Boolean = false


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CompanyViewHolder {
        val binding = DataBindingUtil.inflate<ViewCompanyItemBinding>(
            LayoutInflater.from(viewGroup.context),
            spectrum.global.company.member.R.layout.view_company_item, viewGroup, false
        )
        return CompanyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) =
        holder.bind(employeeFiltered?.get(position)!!, position)


    override fun getItemCount(): Int {
        return employeeFiltered?.size ?: 0
    }

    private fun getItem(position: Int): Companies? {
        return if (position != RecyclerView.NO_POSITION) {
            employeeFiltered?.get(position)
        } else
            null
    }

    fun setEmployeeList(employees: ArrayList<Companies>) {
        this.employees = employees
        this.employeeFiltered = employees
        notifyDataSetChanged()
    }

    fun sortMembers(sortField: String, order: Boolean) {
        when (sortField) {
            "name" -> {
                if (order /*== Sort.DESC*/)
                    employeeFiltered?.sortedWith(compareByDescending { it.company })
                else
                    employeeFiltered?.sortedWith(compareBy { it.company })
                notifyDataSetChanged()
            }
//            "age" -> {
//                if (order/* == Sort.DESC*/)
//                    employeeFiltered?.sortWith(compareByDescending { it.age })
//                else
//                    employeeFiltered?.sortWith(compareBy { it.age })
//                notifyDataSetChanged()
//            }
        }
    }
    inner class CompanyViewHolder(val binding: ViewCompanyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Companies, position: Int) {
            binding.company = item
            val realm = Realm.getDefaultInstance()
            val companyDb =
                realm.where(CompanyDb::class.java).equalTo("companyId", item?.id).findFirst()
            if (companyDb?.isFav == true) {
                binding?.ivFav.setImageResource(R.drawable.ic_favorite_icon)
            } else {
                binding?.ivFav.setImageResource(R.drawable.ic_unfavorite_icon)
            }

            if(companyDb?.isFollow== true){
                binding.ivFollow.text = "Following"
                binding.ivFollow.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.following_button_bg))
            }else{
                binding.ivFollow.text = "Follow"
                binding.ivFollow.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.follow_button_bg))

            }

            binding.ivFav.setOnClickListener {
                val company: CompanyDb = CompanyDb()
                if (isFavorite == false) {
                    binding.ivFav.setImageResource(R.drawable.ic_unfavorite_icon)
                    var realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        company?.id = UUID.randomUUID().toString()
                        company?.companyId = item?.id
                        company.isFav = true
                        realm.copyToRealmOrUpdate(company)
                    }
                    isFavorite = true
                    realm.close()
                } else {
                    binding.ivFav.setImageResource(R.drawable.ic_favorite_icon)
                    var realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        company?.id = UUID.randomUUID().toString()
                        company?.companyId = item?.id
                        company.isFav = false
                        realm.copyToRealmOrUpdate(company)
                    }
                    realm.close()
                    isFavorite = false
                }

            }


            binding.ivFollow.setOnClickListener {
                val company = CompanyDb()
                if (isFollowing) {
                    binding.ivFollow.text = "Follow"
                    binding.ivFollow.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.follow_button_bg))
                    val realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        company?.id = UUID.randomUUID().toString()
                        company?.companyId = item?.id
                        company?.isFollow = true
                        realm.copyToRealmOrUpdate(company)
                    }
                    isFollowing = false
                    realm.close()
                } else {
                    binding.ivFollow.text = "Following"
                    binding.ivFollow.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.following_button_bg))
                    var realm: Realm = Realm.getDefaultInstance()
                    realm.executeTransaction {
                        company?.id = UUID.randomUUID().toString()
                        company?.companyId = item?.id
                        company?.isFollow = false
                        realm.copyToRealmOrUpdate(company)
                    }
                    isFollowing = true
                    realm.close()
                }

            }
        }
    }

    enum class Sort {
        ASC, DESC
    }
//    fun setFollowButton(isSelected:Boolean){
//        if(isSelected){
//
//        }else{
//
//        }
//    }
    override fun getFilter(): Filter {
        return object : Filter() {
            @SuppressLint("DefaultLocale")
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    employeeFiltered = employees
                } else {
                    val filteredList = ArrayList<Companies>()
                    employees?.forEach {
                        if (it.company?.toLowerCase()?.contains(charString.toLowerCase()) == true) {
                            filteredList.add(it)
                        }
                    }
                    employeeFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = employeeFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: Filter.FilterResults
            ) {
                employeeFiltered = null
                employeeFiltered = filterResults.values as ArrayList<Companies>

                notifyDataSetChanged()
            }
        }
    }

}