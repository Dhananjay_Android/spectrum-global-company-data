package spectrum.global.company.member.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import spectrum.global.company.member.ui.viewModel.MainViewModel
import spectrum.global.company.member.data.Companies
import spectrum.global.company.member.databinding.FragmentMemberBinding
import spectrum.global.company.member.ui.adapter.MemberAdapter


class MemberFragment : Fragment() {
    lateinit var binding: FragmentMemberBinding
    private var mainViewModel: MainViewModel? = null
    private var memberAdapter: MemberAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMemberBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val recyclerView = binding.rvMember
        binding.searchEditText.queryHint = "search member"
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        memberAdapter = MemberAdapter()
        recyclerView.adapter = memberAdapter
        getAllMemberData()
        searching(binding.searchEditText)
        binding.toggleButton.setOnCheckedChangeListener { compoundButton, b ->
            memberAdapter?.sortMembers("age", b)

        }
        binding.toggleButtonName.setOnCheckedChangeListener { compoundButton, b ->
            memberAdapter?.sortMembers("name", b)
        }
    }

    private fun getAllMemberData() {
        mainViewModel?.allInfo?.observe(this,
            Observer<List<Companies>> { employees ->
                run {
                    memberAdapter?.setEmployeeList(employees as ArrayList<Companies>)
                }
            })
    }

    override fun onDetach() {
        super.onDetach()
    }

    private fun searching(search: SearchView) {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                memberAdapter?.filter?.filter(newText)
                return true
            }
        })

    }

}
