package spectrum.global.company.member.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import spectrum.global.company.member.ui.fragment.CompanyFragment
import spectrum.global.company.member.ui.fragment.MemberFragment

class HomeAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return CompanyFragment()
            }
            1 -> {
                return MemberFragment()
            }

            else -> return CompanyFragment()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}